This npm library shows my presentation card.

### Usage

```bash
npx mgm793
```

### Output

![alt text](https://gitlab.com/mgm793/presentation-card/-/raw/master/assets/output.png "Output")