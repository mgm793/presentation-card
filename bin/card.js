#!/usr/bin/env node

const boxen = require('boxen');
const chalk = require('chalk');

const logOptions = {
    padding: 1,
    margin: 1,
    borderStyle: 'round'
}

const content = {
    personalInformation: {
        name: chalk.white('Marc Garcia i Mullon'),
        nickname: chalk.green('@mgm793')
    },
    work: {
        title: chalk.white.bold('Work:'),
        text: chalk.white('Software Engineer at Unacast')
    },
    gitlab: {
        title: chalk.white.bold('Gitlab:'),
        text: chalk.green('gitlab.com/mgm793')
    },
    github: {
        title: chalk.white.bold('Github:'),
        text: chalk.green('github.com/mgm793')
    },
    linkedin: {
        title: chalk.white.bold('Linkedin:'),
        text: chalk.green('linkedin.com/in/marcgarciamullon')
    },
    website: {
        title: chalk.white.bold('Website:'),
        text: chalk.green('marcgarciamullon.com')
    }
}

function tranformContentToString(content) {
    const endline = '\n';
    const space = ' ';
    return content.personalInformation.name + ' ( ' + content.personalInformation.nickname + ' )' + endline + endline +
        content.work.title + space + content.work.text + endline +
        content.gitlab.title + space + content.gitlab.text + endline +
        content.github.title + space + content.github.text + endline +
        content.website.title + space + content.website.text + endline +
        content.linkedin.title + space + content.linkedin.text;
}

console.log(boxen(tranformContentToString(content), logOptions));